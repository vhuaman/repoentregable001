package com.techu.entregables;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;
import java.util.ArrayList;
import java.lang.Double;

@RestController
@RequestMapping("/api/v1/productos")
public class ControladorServicios {
    static class Cliente {
        private long id;
        private String nombre;

        public Cliente(long pid, String pnombre)  {
            id = pid;
            nombre = pnombre;
        }


        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
    }

    static class Producto {
        private long id;
        private String marca;
        private String descripcion;
        private Double precio;
        private List<Cliente> clientes;

        public Producto(long pid, String pmarca, String pdescripcion, Double pprecio, List<Cliente> pusuarios)  {
            id = pid;
            marca = pmarca;
            descripcion = pdescripcion;
            precio = pprecio;
            clientes = pusuarios;
        }


        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getMarca() {
            return marca;
        }

        public void setMarca(String marca) {
            this.marca = marca;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public Double getPrecio() {
            return precio;
        }

        public void setPrecio(Double precio) {
            this.precio = precio;
        }

        public List<Cliente> getClientes() {
            return clientes;
        }

        public void setClientes(List<Cliente> clientes) {
            this.clientes = clientes;
        }
    }
//Lista de productos
    private final List<Producto> productos = new ArrayList<Producto>();
    public ControladorServicios(){
        List<Cliente> cliente_lista = new ArrayList<>();
        cliente_lista.add(new Cliente(1,"MANUEL"));
        cliente_lista.add(new Cliente(2,"DANILO"));

        List<Cliente> cliente_lista2 = new ArrayList<>();
        cliente_lista2.add(new Cliente(1,"MELISSA"));
        cliente_lista2.add(new Cliente(2,"PEDRO"));

        List<Cliente> cliente_lista3 = new ArrayList<>();
        cliente_lista3.add(new Cliente(1,"VICTOR"));
        cliente_lista3.add(new Cliente(2,"HANS"));

        productos.add(new Producto(1, "Acer","Laptop",500D, cliente_lista));
        productos.add(new Producto(2, "HP","Impresora",300D, cliente_lista2));
        productos.add(new Producto(3, "Asus","Tablet",250D, cliente_lista3));
    };
// Creando los servicios asociados a la clase
//parte 1
    @GetMapping("/")
    public List<Producto> listProductos() {
        return this.productos;
    }

    @PostMapping("/")
    public List<Producto> listPostProductos() {
        return this.productos;
    }

    //parte 2
    @GetMapping("/{id}")
    public Producto getProducto(@PathVariable long id) {
        for(Producto producto: this.productos) {
            if(producto.id == id)
                return producto;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProducto(@PathVariable long id) {
        for(int i=0; i < this.productos.size(); ++i) {
            Producto producto = this.productos.get(i);
            if(id == producto.id) {
                this.productos.remove(i);
                return;
            }
        }
    }

    @PutMapping("/{id}")
    public void actualizarProducto(@PathVariable long id, @RequestBody Producto productoNuevo) {
        for(int i = 0; i < this.productos.size(); i++) {
            Producto producto = this.productos.get(i);
            if(producto.id == id) {
                producto.marca = productoNuevo.marca;
                producto.descripcion = productoNuevo.descripcion;
                producto.precio = productoNuevo.precio;
                //producto.clientes = productoNuevo.clientes;
                producto.clientes=new ArrayList<>();

                for(int j = 0; j < productoNuevo.clientes.size(); j++) {
                    producto.clientes.add(productoNuevo.clientes.get(j));
                }
                return;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PatchMapping ("/{id}")
        public ResponseEntity patchPrecioProducto(
                @RequestBody Producto actualizarSoloPrecioProd, @PathVariable int id){
            for(int i = 0; i < this.productos.size(); i++) {
                Producto producto = this.productos.get(i);
                if(producto.id == id) {
                    producto.setPrecio(actualizarSoloPrecioProd.getPrecio());
                    return new ResponseEntity<>(producto, HttpStatus.OK);
                }
            }
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
    }
// Parte 3
@GetMapping("/{id}/clientes")
public List<Cliente> getClientes(@PathVariable long id) {
    for(Producto producto: this.productos) {
        if(producto.id == id)
            return producto.getClientes();
    }
    throw new ResponseStatusException(HttpStatus.NOT_FOUND);
}

    @PostMapping("/{id}/clientes")
    public List<Cliente> postClientes(@PathVariable long id, @RequestBody List<Cliente> nuevoCliente) {
        for(Producto producto: this.productos) {
            if(producto.id == id)
            {
              //  List<Cliente> clientes = producto.getClientes();
                producto.clientes=new ArrayList<>();
                for(int j = 0; j < nuevoCliente.size(); j++) {
                    producto.clientes.add(nuevoCliente.get(j));
                }
              return producto.clientes;

            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
//Parte 4
@GetMapping("/{id}/clientes/{idc}")
public Cliente getCliente(@PathVariable long id, @PathVariable long idc) {
    for (Producto producto : this.productos) {
        if (producto.id == id)
            for (Cliente cliente : producto.getClientes()) {
                if (cliente.id == idc)
                    return cliente;
            }
    }throw new ResponseStatusException(HttpStatus.NOT_FOUND);
}


    @DeleteMapping("/{id}/clientes/{idc}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarCliente(@PathVariable long id, @PathVariable long idc) {
        for(Producto producto: this.productos) {
            if (id == producto.id) {
                for (int j = 0; j < producto.getClientes().size(); j++) {
                    Cliente cliente = producto.getClientes().get(j);
                    if (cliente.id == idc) {
                        producto.getClientes().remove(j);
                        //  producto.getClientes().removeIf(e -> e.id == idc);
                        return;
                    }
                }
            }
        }
    }

    @PatchMapping ("/{id}/cliente/{idc}")
    public ResponseEntity patchNombreCliente(
            @RequestBody Cliente actualizarNombreCliente, @PathVariable int id, @PathVariable int idc){
        for(int i = 0; i < this.productos.size(); i++){
            Producto producto = this.productos.get(i);
            if(producto.id == id) {
                for(int j = 0; j <producto.getClientes().size(); j++){
                    if(producto.getClientes().get(j).id == idc){
                        producto.getClientes().get(j).setNombre(actualizarNombreCliente.getNombre());
                        return new ResponseEntity<>(producto, HttpStatus.OK);
                    }
                }
                return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);

            }
        }
        return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}/cliente/{idc}")
    public void actualizarCliente(@PathVariable long id, @PathVariable long idc, @RequestBody Cliente clienteActualizado) {
        for(int i = 0; i < this.productos.size(); i++) {
            Producto producto = this.productos.get(i);
            if(producto.id == id) {
 /*               producto.marca = productoNuevo.marca;
                producto.descripcion = productoNuevo.descripcion;
                producto.precio = productoNuevo.precio;*/
                //producto.clientes = productoNuevo.clientes;
               // producto.clientes=new ArrayList<>();

                for(int j = 0; j <producto.getClientes().size(); j++){
                    if(producto.getClientes().get(j).id == idc){
                        producto.getClientes().set(j,clienteActualizado);
               //         return new ResponseEntity<>(producto, HttpStatus.OK);
                    }
                }
                return;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
}
